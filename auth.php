<?php
//Iekļaujam failu, kurš satur konfigurāciju savineojumam ar datubāzi
include('include/db.php');

//Šis fails meklēs vai šāds lietotājs vspār eksistē, ja jā, tad pāries uz index lapu, lai varētu sākt strādāt.

//Saņemam visus mainīgos no formas

$parole = mysqli_real_escape_string($conn, $_POST['parole']);
$epasts = mysqli_real_escape_string($conn, $_POST['epasts']);

//atrodam lietotājvārdu un paroli datubāzē.

$meklejam = "SELECT UsrMail, UsrParole, UsrDzests,UsrAdmin FROM tbLietotaji WHERE UsrMail = '$epasts' AND UsrDzests = 0";
$izpildam = mysqli_query($conn, $meklejam);
while($rinda = mysqli_fetch_assoc($izpildam))
{
    $paroles_h = $rinda['UsrParole'];
    //Pie reizes paskatamies, vai tas, kurš ielogojas ir admins.
    $iradmins = $rinda['UsrAdmin'];
}
//Pārbaudām vai sakrīt paroļu hash informācija

if (password_verify($parole, $paroles_h)) {
//Ja paroles sakrīt, sākam sesiju reģistrējam globālos mainīgos un priecājamies.
    //iztīrām, ja nu kas palicis no iepriekšējās sesijas.
    session_unset(); 
    //Sākam jaunu svaigu sesiju
    session_start();
    //Paņemsim sesijai pāris mainīgos
    $_SESSION['mails'] = $epasts;
    $_SESSION['admin'] = $iradmins;
    //Ielādējam sākuma lapu, jeb index.php
    header("Location: index.php");
    die();
} else {
    //Ja parole nav pareiza, tad beidzam visas sesijas.
    // Novācam visus sesiju mainīgos
	session_unset(); 
	// iznīcinām sesiju
	session_destroy(); 
    header("Location: login.php");
   	die();
}
?>