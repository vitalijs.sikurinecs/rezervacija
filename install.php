<!DOCTYPE html>
<html>
<title>Rezervācija</title>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1">
<!--Šeit norādīts ceļš uz w3 school CSS var dzēst vai labot pēc savas vajadzības-->
<link rel="stylesheet" href="css/w3.css">
<?php
//Šeit jānorāda ceļš uz db.php failu. Tiks veikta pārbaude, vai šāds fails eksistē.
//Ja eksistē, tad skripts rakstīts, ka instalācija veiksmīga! Trūkums tāds, ka db.php var būt nekorekti izveidots.
if(include('include/db.php'))
  {    
    echo '<h2>Instalācija ir veikta veiksmīgi! Ja nepieciešams pārinstalēt, dzēsiet datubāzi!</h2>';      
  }
else
{
  //Ja fails netika atrasts, tad paziņos, ka jāveic instalācija.
  echo '<h2>Nav izveidots savienojums ar DB. Lūgums to konfigurēt zemāk</h2>';

//Sāksim ar Datubāzes savienojuma konfigurēšanu. ar echo komandu izvada HTML kodu formas izveidošanai.
echo '<div class="w3-container w3-padding-16">';
echo '<div class="w3-third w3-container w3-padding-16">';
echo '<h3>1. solis:Konfigurējiet datubāzes parametrus</h3>';
echo '<form action="" method="POST">';
echo '<label>Datubāzes serveris</label>';
echo '<input class="w3-input" type="text" required name="server">';
echo '<label>Datubāzes lietotājvārds</label>';
echo '<input class="w3-input" type="text" required name="user">';
echo '<label>Datubāzes parole</label>';
echo '<input class="w3-input" type="text" required name="password">';
echo '<label>Datubāzes nosaukums</label>';
echo '<input class="w3-input" type="text" required name="dbname"><br>';
echo '</div>
<div class="w3-padding-16 w3-container w3-third">
<h3>2.Solis:Pievienot uzņēmuma lietotāju:</h3>
<form action="" method="POST">
<label>Epasts:</label>
<input class="w3-input" type="mail" required name="epasts">
<label>Parole:</label>
<input class="w3-input" type="password" required name="parole">
<br>
<input type="submit" class="w3-button w3-green" name="db_saglabat" value="Saglabāt">
</form>
</div>
</div>';
    //Ja formā ir nospiesta poga saglabāt izpildās viss zemāk minētais kods.
    //Izveidojam db.php failu
    if(isset($_POST['db_saglabat']))
    {
      //dabūjam DB parametrus
    $servername1 = $_POST['server'];
    $username1 = $_POST['user'];
    $password1 = $_POST['password'];
    $dbname1 = $_POST['dbname'];
    // Create connection
    $conn1 = mysqli_connect($servername1, $username1, $password1, $dbname1);
    mysqli_set_charset( $conn1, 'utf8');
    // Check connection
    if (!$conn1) {
        die('Connection failed: ' . mysqli_connect_error());
    }
  
    //skripts, kurš izveidos tabulas iekš dBase 
    //saglabājam DB
    //Tabula priekš lietotajiem. Nedzēsiet
    $tabulausers="CREATE TABLE IF NOT EXISTS tbLietotaji (
        UsrID int(25) NOT NULL AUTO_INCREMENT,
        UsrMail text COLLATE utf8_bin NOT NULL,
        UsrParole text COLLATE utf8_bin NOT NULL,
        UsrAdmin int(2) NOT NULL DEFAULT 0,
        UsrDzests int(2) NOT NULL DEFAULT 0,
        PRIMARY KEY (UsrID)
      ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;";
    //PIEVIENOJIET KODU SAVU TABULU IZVEIDEI!
    $tabulaklienti="CREATE TABLE IF NOT EXISTS tbKlienti (
      KlientaID int(5) NOT NULL AUTO_INCREMENT,
      KlientaVards text COLLATE utf8_bin NOT NULL,
      KlientaUzvards text COLLATE utf8_bin NOT NULL,
      KlientaEpasts text COLLATE utf8_bin NOT NULL,
      KlientaTel text COLLATE utf8_bin NOT NULL,
      PRIMARY KEY (KlientaID)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;";
    $tabulapakalpojumi="CREATE TABLE IF NOT EXISTS tbPakalpojumi (
      PakID int(5) NOT NULL AUTO_INCREMENT,
      PakNosaukums TEXT NOT NULL,
      PakIlgums int(5) NOT NULL,
      PakPieejams int(2) NOT NULL DEFAULT 1,
      PRIMARY KEY (PakID)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;";
  $tabularezervacija="CREATE TABLE IF NOT EXISTS tbRezervacija (
      RezID int(5) NOT NULL AUTO_INCREMENT,
      KlientaRezID int(5) NOT NULL,
      PakalpRezID int(5) NOT NULL,
      RezPiezime text COLLATE utf8_bin,
      RezDatums text NOT NULL,
      RezApstiprinats int(2) NOT NULL DEFAULT 0,
      RezAtcelts int(2) NOT NULL DEFAULT 0,
      RezNoticis int(2) NOT NULL DEFAULT 0,
      PRIMARY KEY (RezID)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;";
  

    //MySQL VAICĀJUMU IZPILDEI! Skatīt video. Ierakstiet savu vaicājumu izpildi. Improvise Adapt Overcome!!!
    
    
    $parole= mysqli_real_escape_string($conn1, $_POST['parole']);
    $parolehash=password_hash($parole, PASSWORD_DEFAULT);
    $epasts=mysqli_real_escape_string($conn1, $_POST['epasts']);
    //Sagatavojam vaicājumu 1. lietotāja izveidei (aizpidlīts formā)
    $insusr="INSERT INTO tbLietotaji (UsrMail,UsrParole,UsrAdmin) VALUES ('$epasts','$parolehash',1)";
    /*Pārbaudam, vai varam izveidot savienojumu ar SQL serveri un vai varam izveidot 1. tabulu.
    Ja varam, tad turpinam instalācijas gaitu, ja nē, izvadam kļūdu!
    */
    if(mysqli_query($conn1,$tabulausers))
    {
    //Izveidojam db.php failu
      //Norādam faila atrašanās vietu! Mapei ir jāeksistē uz servera!      
      $jaunsFails = 'include/db.php';
      //Pārbaudam, vai norādītā mapē var izveidot failu!
      if ( ! is_writable(dirname($jaunsFails))) {
          /*Ja nav iespējams izveidot failu, kļūdas paziņojums! Mapei jābūt rakstāmai www-data lietotājam!
          To arī norādam kļūdas paziņojumā. Komanda serverī, lai veiktu izmaīnas chown -R www-data:www-data /cels/uz/mapi
          */
          echo dirname($jaunsFails) . ' jābūt rakstāmam <b>www-data!!!</b>';
      } else {
      //Sagatavojam db.php faila saturu  
      $failaSaturs= '<?php
      $servername="'.$_POST['server'].'"; 
      $username="'.$_POST['user'].'";
      $password="'.$_POST['password'].'";
      $dbname="'.$_POST['dbname'].'";
      $conn = mysqli_connect($servername, $username, $password, $dbname);
      mysqli_set_charset( $conn, "utf8");
      if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
      }
      ?>';
      //Saglabājam failu
      file_put_contents($jaunsFails, $failaSaturs);
      //Izpildām visus SQL vaicājumus, lai izveidotu un saglabātu datus datubāzē
      mysqli_query($conn1,$insusr);
      mysqli_query($conn1,$tabulaklienti);
      mysqli_query($conn1,$tabulapakalpojumi);
      mysqli_query($conn1,$tabularezervacija);
    //Paziņojam, ka instalācija veiksmīga  
    echo '<h2>Instalācija ir veiksmīga! <a href=index.php>Sākums</a></h2>';
    
   }
  }
    else
    {
    /*Ja nu tomēr kaut kas noiet greizi. Kļūdas paziņojums. 
    Problēmu meklēt LOG failos, vai pamainam kodu, lai tiktu attēlotas kļūdas.*/  
    echo '<h2>Kaut kas nogāja greizi. Sazinieties ar izstrādātāju!</h2>';
    }
    
    
  }
};



