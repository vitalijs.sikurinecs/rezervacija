<!DOCTYPE html>
<html>
<title>Reģistrēties</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/w3.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<body>

<form action="" method="post" class="w3-container w3-card-4 w3-light-grey w3-text-blue w3-margin">
<h2 class="w3-center">Reģistrēties</h2>
 
<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-user"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="vards" type="text" autocomplete="off" required placeholder="Vārds">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-user"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="uzvards" type="text" autocomplete="off" required placeholder="Uzvārds">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-envelope-o"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="epasts" type="text" autocomplete="off" required placeholder="epasts">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-phone"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" required name="telefons" autocomplete="off" type="text" placeholder="Telefona nr.">
    </div>
</div>

<div class="w3-row w3-section">
  <div class="w3-col" style="width:50px"><i class="w3-xxlarge fa fa-lock"></i></div>
    <div class="w3-rest">
      <input class="w3-input w3-border" name="parole" autocomplete="off" type="password" required>
    </div>
</div>

<p class="w3-center">
<input type="submit" name="registreties" class="w3-button w3-section w3-blue w3-ripple" value="Reģistrēties">
</p>
</form>
<?php
include('include/db.php');
if(isset($_POST['registreties']))
{
    //Ja nospiesta poga reģistrēties, sagatavosim visus ievadītos laukus saglabāšanai DB.
    $vards= mysqli_real_escape_string($conn, $_POST['vards']);
    $uzvards=mysqli_real_escape_string($conn, $_POST['uzvards']);
    $epasts=mysqli_real_escape_string($conn, $_POST['epasts']);
    $telefons=mysqli_real_escape_string($conn, $_POST['telefons']);
    $parole=mysqli_real_escape_string($conn, $_POST['parole']);
    $parolehash=password_hash($parole, PASSWORD_DEFAULT);
    //Veidojam vaicājumu, lai izveidotu lietotāju. kurš var ielogoties sistēmā.
    $register="INSERT INTO tbLietotaji (UsrMail,UsrParole,UsrAdmin) VALUES ('$epasts','$parolehash',0);";
    //Veidojam vaicājumu, kurš saglabās lietotaja datus tabulā Klienti
    $pievlietotaju = "INSERT INTO tbKlienti (KlientaVards,KlientaUzvards,KlientaEpasts,KlientaTel) VALUES ('$vards','$uzvards','$epasts','$telefons');";
    //Izpildām abus vaicājumus ar nosacījumiem, lai paziņotu par kļūdu

    if(mysqli_query($conn,$register))
    {
        if(mysqli_query($conn,$pievlietotaju))
        {
            //Ja izdevušās abas darbības, paziņojam, ka reģistrācija veiksmīga un varam doties ielogoties.
            echo '<h2>Reģistrācija ir veiksmīga. Varat ielogoties izmantojot savu epastu un paroli <a href=login.php>šeit</a></h2>';
        }   
        else
        {
            echo '<h3>Neizdevās saglabāt datus par klientu!</h3>';
            echo mysqli_errno($conn);
        }
    }
    else
    {
        echo '<h3>Neizdevās reģistrēt lietotaju!</h3>';
    }
    

}

?>
</body>
</html> 