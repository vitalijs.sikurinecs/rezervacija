<?php
    if (session_status() == PHP_SESSION_NONE) {
    session_start();
	}
    // Novācam visus sesiju mainīgos
	session_unset(); 
	// iznīcinām sesiju
	session_destroy(); 
    header("Location: login.php");
	die();
?>