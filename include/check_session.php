<?php
//Pārbaudām vai sesija ir sākusies. 
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
//Ja sesija ir sākusies, bet nesatur pareizos mainīgos, tad iznīcinam to.
if ($_SESSION['mails'] == ''){
        header("Location: login.php");
        die();
}
/* Autora piebilde. Šī ir ļoti primitīva sesijas drošības pārbaude.
Pēc būtības būtu jāģenerē unikāls sesijas identifikators, tas kaut kur jāglabā uz servera un jāsalīdzina ar to, kas
ir klienta pusē. Ja mainās, sesija ir jāiznīcina.
Bet šim vienkāršajam darbam derēs arī šādi. Kaut gan viegli laužams variants.
*/
?>