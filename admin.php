<!DOCTYPE html>
<html>
<head>
<title>Rezervācija</title>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1">
<!--Šeit norādīts ceļš uz w3 school CSS var dzēst vai labot pēc savas vajadzības-->
<link rel="stylesheet" href="css/w3.css">
</head>
<?php
include('include/menu.php');
//Administrācijas lapa. Pēc būtības šeit var pieslēgties lietotajs tikai ar administratora tiesībām.
//Pievienojam sesijas pārbaudi un pievienojam datubāzes knfiguracijas failu db.php
require('include/check_session.php');
require('include/db.php');
//Vajadzētu pārbaudīt vai ir administrators, ja nav, tad lapu nevar apmeklēt.
if($_SESSION['admin']==1)
{
    //ja ir admins, tad ir ok
    //Vajadzētu attēlot un ot iespēju pievienot sekojošas lietas:
    /*
        1. Pievienot pakalpojumus.
        2. Apskatīt pieteiktās rezervācias
        3. Apstiprināt tās
        4. Atzīmēt kā pabeigtas
        5. Atzīmēt kā atceltas (šo vajadzētu ļaut arī darīt lietotājam parastaja)
        */
    //Iespēja pievienot jaunus pakalpojumus

    echo'<div class="w3-container">
        <h2>Administrācijas panelis</h2>
        <p class="w3-large">Šeit Jūs varat pārvaldīt pakalpojumus un reģistrācijas.</p>
    </div>';
    echo '<div class="w3-row">';   
    echo '<div class="w3-rest">';
    echo '<h3>Pieteiktie pakalpojumi</h3>
    <table class="w3-table w3-striped">
    <tr>
        <td>Nr.pk.</td>
        <td>Pakalpojums</td>
        <td>Vārds</td>
        <td>Uzvārds</td>
        <td>Telefons</td>
        <td>E-pasts</td>
        <td>Rez. dat.</td>
        <td>Pabeigts.</td>
        <td>Atcelts</td>
    </tr>';
    //Vajadzēs vaicajumu, kurš atrod datubāzē atbilstošu informāciju + dinamiskas formas, kuras ļaus
    //labot rezervācijas statusu.
    //Meklēsim visus ierakstus un kārtosim pēc datuma dilstošā secībā, lai jaunākie ir augšpusē.
    //Būs jāsasaista 2 tabulas. tbKlienti, tbRezevacija, lai iegūtu datus par klientu.
    $meklejamRez = "SELECT * FROM tbRezervacija INNER JOIN tbKlienti ON tbRezervacija.KlientaRezID = tbKlienti.KlientaID INNER JOIN tbPakalpojumi ON tbRezervacija.PakalpRezID = tbPakalpojumi.PakID ORDER BY RezDatums DESC;";
    $nrpk = 1;
    $meklejamRez = mysqli_query($conn,$meklejamRez);
    while($rorez=mysqli_fetch_assoc($meklejamRez)){
        //ciklā aizpildam tabulu
       echo '<tr>';
       echo '<td>'.$nrpk.'</td>';
       echo '<td>'.$rorez['PakNosaukums'].'</td>';
       echo '<td>'.$rorez['KlientaVards'].'</td>';
       echo '<td>'.$rorez['KlientaUzvards'].'</td>';
       echo '<td>'.$rorez['KlientaTel'].'</td>';
       echo '<td>'.$rorez['KlientaEpasts'].'</td>';
       echo '<td>'.$rorez['RezDatums'].'</td>';
       //pārbaudīšim vai jābūt atzīmētiem ķekšiem
       if($rorez['RezNoticis']==1){
        echo '<td><input type="checkbox" checked></td>';
       }
       else{
        echo '<td><input type="checkbox" ></td>';   
       }
       if($rorez['RezAtcelts']==1){
        echo '<td><input type="checkbox" checked></td>';
       }
       else{
        echo '<td><input type="checkbox" ></td>';   
       }
       echo '</tr>';
       $nrpk++;        
    }
    echo'</table>';
    
    echo '</div>'; 
    echo '</div';
    //Otra rinda
    echo '<div class="w3-row">'; 
    echo '<div class="w3-twothird">';
    echo '<h3>Pārvaldīt pakalpojumus</h3>
    <h4>Pievienot jaunu pakalopjumu</h4>
    <form action="" method="POST">
    <input class="w3-input w3-border" name="pakalpojums" type="text" autocomplete="off" required placeholder="Pakalpojuma nosaukums">
    <input class="w3-input w3-border" name="pakIlgums" type="text" autocomplete="off" required placeholder="Pak. norises ilgums minūtēs">
    <input type="submit" name="pakSaglabat" class="w3-button w3-section w3-blue w3-ripple" value="Pievienot">
    </form>
    <h4>Pieejamie pakalpojumi</h4>
    <table class="w3-table w3-striped">
    <tr>
        <td>Nr.pk.</td>
        <td>Pakalpojums</td>
        <td>Ilgums</td>
        <td>Pieejams</td>
    </tr>';
    //Vajadzēs vaicājumu datubāzei, lai iegūtu visu info un ļāut labot pieejamību.
    //atrodam pakalpojumus (arī neaktīvos)
    $nrpk = 1;
    $attPakalp = "SELECT * FROM tbPakalpojumi";
    $meklejamPak = mysqli_query($conn,$attPakalp);
    //Cikls, kurš izvadīs visus rezultātus
    while($rowpak=mysqli_fetch_assoc($meklejamPak))
    {
       echo '<tr>';
       echo '<td>'.$nrpk.'</td>';
       echo '<td>'.$rowpak['PakNosaukums'].'</td>';
       echo '<td>'.$rowpak['PakIlgums'].'</td>';
       if($rowpak['PakPieejams']==1){
        echo '<td><input type="checkbox" checked></td>';
       }
       else{
        echo '<td><input type="checkbox"></td>';   
       }
       echo '</tr>';
       $nrpk++;
    }  
    echo'</table>';
    
    echo '</div>'; 
}
else
{
    //ja nav, tad vienkārši metam prom lietotāju uz sākuma lapu.
    //Ja būtu log, varētu arī ierakstīt, kurš šeit ložņā.
    echo '<h1>Jums nav administratora tiesības!</h1>';
    header("refresh:4;url=index.php");
}
//Ja nospiedīs pogu pievienot pakalpojumu
if(isset($_POST['pakSaglabat'])){
    $paknosaukums = mysqli_real_escape_string($conn, $_POST['pakalpojums']);;
    $pakilgums = mysqli_real_escape_string($conn, $_POST['pakIlgums']);;
    $vaicajums = "INSERT INTO tbPakalpojumi (PakNosaukums,PakIlgums) VALUES ('$paknosaukums',$pakilgums);";
    //izpildam vaicājumu ar testu
    if(!mysqli_query($conn,$vaicajums)){
        die("Connection failed: " . mysqli_connect_error());
    }
    else
    {
        header("location:admin.php");
    }

}
?>