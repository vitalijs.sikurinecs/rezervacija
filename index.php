<!DOCTYPE html>
<html>
<?php
//pārbaudām vai ir aktīva sesija!
require_once('include/check_session.php');
include('include/db.php');
//Atradīsim kāds šim lietotājam ir klienta ID, Vārds, uzvards.
$mails = $_SESSION['mails'];
$mekldatus = "SELECT * FROM tbKlienti WHERE KlientaEpasts ='$mails'; ";
$rez = mysqli_query($conn,$mekldatus);
while($rowdati=mysqli_fetch_assoc($rez))
{
    $sesijasvards=$rowdati['KlientaVards'];
    $sesijasuzvards=$rowdati['KlientaUzvards'];
    $sesLietId=$rowdati['KlientaID'];
}
?>
<head>
<title>Rezervācijas APP</title>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1">
<!--Šeit norādīts ceļš uz w3 school CSS var dzēst vai labot pēc savas vajadzības-->
<link rel="stylesheet" href="css/w3.css">
</head>
 <?php include('include/menu.php');?>
<div class="w3-container w3-padding-16">
<h2>Esat sveicināts <?php echo ' '.$sesijasvards.' '.$sesijasuzvards;?></h2>
<h3>Jūsu rezervācijas</h3>
<!-- Šeit no DB atlasīt vias rezervācijas ar šī klienta ID -->
<?php
//Vajag izvadīt tabulu ar veiktajām rezervācijām tieši šim klientam.
echo '<h3>Pieteiktie pakalpojumi</h3>
    <table class="w3-table w3-striped">
    <tr>
        <td>Nr.pk.</td>
        <td>Pakalpojums</td>
        <td>Telefons</td>
        <td>E-pasts</td>
        <td>Rez. dat.</td>
        <td>Atcelts</td>
    </tr>';
//vaicājums
$meklejamRez = "SELECT * FROM tbRezervacija INNER JOIN tbKlienti ON tbRezervacija.KlientaRezID = tbKlienti.KlientaID INNER JOIN tbPakalpojumi ON tbRezervacija.PakalpRezID = tbPakalpojumi.PakID WHERE KlientaRezID = $sesLietId AND RezNoticis=0 ORDER BY RezDatums DESC;";
    $nrpk = 1;
    $meklejamRez = mysqli_query($conn,$meklejamRez);
    while($rorez=mysqli_fetch_assoc($meklejamRez)){
        //ciklā aizpildam tabulu
       echo '<tr>';
       echo '<td>'.$nrpk.'</td>';
       echo '<td>'.$rorez['PakNosaukums'].'</td>';
       echo '<td>'.$rorez['KlientaTel'].'</td>';
       echo '<td>'.$rorez['KlientaEpasts'].'</td>';
       echo '<td>'.$rorez['RezDatums'].'</td>';
       //pārbaudīšim vai jābūt atzīmētiem ķekšiem
       if($rorez['RezAtcelts']==1){
        echo '<td><input type="checkbox" checked></td>';
       }
       else{
        echo '<td><input type="checkbox" ></td>';   
       }
       echo '</tr>';
       $nrpk++;        
    }
    echo'</table>';

?>
<h3>Izveidot jaunu rezervāciju</h3>
<!--Vajadzēs kalendāru un vajadzēs no datubāzes atrast visus pakalpojumus un ielikt drop down izvēlnē-->
<form action="" method="POST">
    <label>Izvēlies pakalpojumu</label>
    <select required name="pakalpojums" id="pakalpojums">
        <option></option>
<?php
//Atradīsim visus pakalpojumus, kuri ir atzīmēti kā pieejami.
$attPakalp = "SELECT * FROM tbPakalpojumi WHERE PakPieejams=1";
$meklejamPak = mysqli_query($conn,$attPakalp);
//Cikls, kurš izvadīs visus rezultātus
while($rowpak=mysqli_fetch_assoc($meklejamPak))
{
    echo '<option value="'.$rowpak['PakID'].'">'.$rowpak['PakNosaukums'].' '.$rowpak['PakIlgums'].'min</option>';
}
echo '</select>';
?>
<input type="text" name="piezime" placeholder="Piezīme">
<!-- kalendārs -->
<?php  
//ļausim rezervēt ar nākamo dienu, lai var paspēt apstrādāt.
$nakamadiena = date("Y-m-d", strtotime('+1day'));
//neļausim rezrevēt talāk kā 14 dienas uz priekšu. Lai var organizēt darbu.
$maksdat = date("Y-m-d",strtotime('+15 day'));
       echo '<input name="rezdat" type="date" min="'.$nakamadiena.'" max="'.$maksdat.'" value="'.$nakamadiena.'">';

//Vēl vajag slēpto lauku, lai zinātu, kurš rezervē. 

echo '<input type="hidden" name="rez_lietotajs" value="'.$sesLietId.'">';
?>
<input type="submit" class="w3-button w3-green" name="rezervet" value="Rezervēt">
</form>
<?php
if($_GET['error']==='none')
{
    echo 'Rezervācija notikusi veiksmīgi!';
}
?>
</div>

<?php
//saglabāsim datus tabulā rezervācijas.
if(isset($_POST['rezervet']))
{
    $rez_dat=$_POST['rezdat'];
    $rez_kl_id=$_POST['rez_lietotajs'];
    $rez_pak_id=$_POST['pakalpojums'];
    $rez_piezime=mysqli_real_escape_string($conn1,$_POST['piezime']);
    $sqlPiepr= "INSERT INTO tbRezervacija (KlientaRezID,PakalpRezID,RezPiezime,RezDatums) VALUES ($rez_kl_id,$rez_pak_id,'$rez_piezime','$rez_dat');";

    
    
    //izpildām
    if(!mysqli_query($conn,$sqlPiepr)){
        die("Connection failed: " . mysqli_connect_error());
        exit();
    }
    else
    {
        header("location:index.php?error=none");
    }


}
?>

